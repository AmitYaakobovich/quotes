package Main;

import Main.Authentication.Auth;
import Main.dataObjects.QuoteObject;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import static Main.QuotesService.AllQuotes;

/**
 * Created by amityaa on 21/02/2018.
 */

@RestController
public class QuotesController {

    private final static Logger logger = Logger.getLogger(QuotesController.class);

    @CrossOrigin
    @RequestMapping(path = "getQuotesList", method = RequestMethod.GET)
    public String getQuotesList(@RequestHeader(value = "X-Auth") String token){

        logger.info("getting quotes list...");

        if (token.equals(Auth.getPass())){
            return String.valueOf(AllQuotes);
        }
        else {
            return "No Privileges";
        }
    }

    @CrossOrigin
    @RequestMapping(path = "suggestQuote", method = RequestMethod.POST)
    public String suggestQuote(@RequestBody QuoteObject quoteObject) {

        logger.info("suggested quote: "+ quoteObject);

        try {
            QuotesService.addQuoteObject(quoteObject);
        }catch (Exception e){
            return "Failed: " + e.getMessage();
        }
        return "Done";
    }

    @CrossOrigin
    @RequestMapping(path = "getRandomQuote", method = RequestMethod.GET)
    public String getRandomQuote(){

        int numOfQuotes = AllQuotes.size();
        int randomNum = ThreadLocalRandom.current().nextInt(0, numOfQuotes);
        /*while (AllQuotes.get(randomNum) == null){
            randomNum = (randomNum + 1) % numOfQuotes;
        }*/
        return AllQuotes.get(randomNum).getText();
    }

    @CrossOrigin
    @RequestMapping(path = "reloadQuotesList", method = RequestMethod.GET)
    public String reloadQuotesList(@RequestHeader(value = "X-Auth") String token) throws IOException {
        if (token.equals(Auth.getPass())){
            QuotesService.reloadQuotesList();
        }
        else {
            return "No Privileges";
        }
        return "Done";
    }

    /*@CrossOrigin
    @RequestMapping(path = "updateQuotesList", method = RequestMethod.GET)
    public String updateQuotesList(@RequestHeader(value = "X-Auth") String token) throws IOException {
        if (token.equals(Auth.getPass())){
            QuotesService.updateQuotesList();
        }
        else {
            return "No Privileges";
        }
        return "Done";
    }*/
}
