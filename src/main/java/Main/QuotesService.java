package Main;

import Main.dataObjects.QuoteObject;
import Main.dataObjects.QuotedBy;
import Main.dataObjects.QuotedByTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by amityaa on 21/02/2018.
 */

@Service
public class QuotesService {

    private final static Logger logger = Logger.getLogger(QuotesService.class);

    @Autowired
    static
    List<QuoteObject> AllQuotes = Collections.synchronizedList(new ArrayList<QuoteObject>());

    @Autowired
    static
    ConcurrentHashMap<String, QuoteObject> QuotesByQuotedByName = new ConcurrentHashMap<>();

    @Autowired
    static
    ConcurrentHashMap<String, QuoteObject> QuotesByQuotedByType = new ConcurrentHashMap<>();

    @Autowired
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat
            ("dd/MM/yyyy HH:mm:ss");

    public static void reloadQuotesList() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("src\\main\\java\\Main\\TextFiles\\AllQuotes.txt"))) {
            String line;
            while ((line = br.readLine()) != null && !line.isEmpty()) {
                String[] params = line.split("|");
                QuoteObject quoteObject = new QuoteObject();
                quoteObject.setQuoteId(params[0]);
                quoteObject.setText(params[1]);
                quoteObject.setQuotedBy(new QuotedBy(params[2], QuotedByTypes.convertFromString(params[3])));
                quoteObject.setEntryDate(params[4]);
                if (params.length == 6){
                    quoteObject.setQuoteSource(params[5]);
                }
                if (params.length == 7){
                    quoteObject.setImageFileName(params[6]);
                }
                AllQuotes.add(quoteObject);
            }
        }
    }

    public static void updateQuotesList(QuoteObject quoteObject) throws IOException {
        File fout = new File("AllQuotes.txt");
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        bw.write(quoteObject.getQuoteId() + "|" + quoteObject.getText() + "|" +
                quoteObject.getQuotedBy().getName() + "|" + quoteObject.getQuotedBy().getType()
        + "|" + quoteObject.getEntryDate());
        bw.newLine();

        bw.close();
    }

    public static void addQuoteObject(QuoteObject quoteObject){

        QuoteObject newQuoteObject = new QuoteObject(quoteObject.getText(), quoteObject.getQuotedBy());
        AllQuotes.add(newQuoteObject);
        try {
            updateQuotesList(newQuoteObject);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }
}
