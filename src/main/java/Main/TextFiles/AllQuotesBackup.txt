When you have to shoot, shoot. Don't talk.
don't get high on your own supply
don't do the crime if you can't do the time
Life is full of misery, loneliness, and suffering - and it's all over much too soon.
My one regret in life is that I am not someone else.
beer is the proof that god loves us and wants us to be happy
Bisexuality immediately doubles your chances for a date on Saturday night.
There are worse things in life than death. Have you ever spent an evening with an insurance salesman?
I'm not afraid to die, I just don't want to be there when it happens.
Seventy percent of success in life is showing up.
Sex without love is a meaningless experience, but as far as meaningless experiences go its pretty damn good.
I don't want to achieve immortality through my work. I want to achieve it through not dying.
I failed to make the chess team because of my height.
Basically my wife was immature. I'd be at home in the bath and she'd come in and sink my boats.
I have bad reflexes. I was once run over by a car being pushed by two guys.
Life is divided into the horrible and the miserable.
If only God would give me some clear sign! Like making a large deposit in my name at a Swiss bank.
You can live to be a hundred if you give up all the things that make you want to live to be a hundred.
In Beverly Hills... they don't throw their garbage away. They make it into television shows.
Money is better than poverty, if only for financial reasons.
If my films don't show a profit, I know I'm doing something right.
My feeling about death? I'm against it.
I'm very proud of my gold pocket watch. My grandfather, on his deathbed, sold me this watch.
I'm astounded by people who want to 'know' the universe when it's hard enough to find your way around Chinatown.
I believe there is something out there watching us. Unfortunately, it's the government.
Not only is there no God, but try finding a plumber on Sunday.
Most of the time I don't have much fun. The rest of the time I don't have any fun at all.
I don't believe in the after life, although I am bringing a change of underwear.
I think being funny is not anyone's first choice.
In my house I'm the boss, my wife is just the decision maker.
The problem is, God gave man a brain and a penis and only enough blood to run one at a time
I will not eat oysters. I want my food dead. Not sick. Not wounded. Dead.
When I was kidnapped, my parents snapped into action. They rented out my room.
There are only three things women need in life: food, water, and compliments.
Always remember that you are absolutely unique. Just like everyone else.
I may be drunk, Miss, but in the morning I will be sober and you will still be ugly.
If women ran the world we wouldn't have wars, just intense negotiations every 28 days.
No man has a good enough memory to be a successful liar.
Get your facts first, then you can distort them as you please.
If you're going to do something tonight that you'll be sorry for tomorrow morning, sleep late.
Weather forecast for tonight: dark.
A woman's mind is cleaner than a man's: She changes it more often.
We are all here on earth to help others; what on earth the others are here for I don't know.
There cannot be a crisis next week. My schedule is already full.
Go to Heaven for the climate, Hell for the company.
Wine is constant proof that God loves us and loves to see us happy.
Laugh and the world laughs with you, snore and you sleep alone.
Happiness is having a large, loving, caring, close-knit family in another city.
I believe that if life gives you lemons, you should make lemonade... And try to find somebody whose life has given them vodka, and have a party.
Laziness is nothing more than the habit of resting before you get tired.
I can resist everything except temptation.
Smoking kills. If you're killed, you've lost a very important part of your life.
