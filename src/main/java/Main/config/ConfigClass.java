package Main.config;

import Main.QuotesService;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created by amityaa on 07/05/2018.
 */

@Configuration
public class ConfigClass {

    private final static Logger logger = Logger.getLogger(ConfigClass.class);

    @Bean
    public QuotesService quotesServiceConfig(){
        return new QuotesService();
    }

    @Bean
    public String reloadQuotes(){

        logger.info("reloading quotes...");

        try {
            QuotesService.reloadQuotesList();
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed to reload quotes!";
        }

        return "Quotes were reloaded successfully";
    }
}
