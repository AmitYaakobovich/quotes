package Main.dataObjects;

import Main.QuotesService;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

/**
 * Created by amityaa on 13/03/2018.
 */

public class QuoteObject {

    public QuoteObject() {

    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    private String quoteId = "";

    @NotBlank
    @Size(min = 10, max = 100)
    private String text;
    @NotBlank
    private QuotedBy quotedBy;

    public String getQuoteSource() {
        return quoteSource;
    }

    public void setQuoteSource(String quoteSource) {
        this.quoteSource = quoteSource;
    }

    private String quoteSource = "";
    private String entryDate;
    private String imageFileName;

    public QuoteObject(String text, QuotedBy quotedBy){
        this.text = text;
        this.quotedBy = quotedBy;
        this.entryDate = QuotesService.dateFormat.format(new Date());
        this.setQuoteId(UUID.randomUUID().toString().substring(0,15));
    }

    public QuoteObject(String text, QuotedBy quotedBy, String imageFilePath){
        this.text = text;
        this.quotedBy = quotedBy;
        this.entryDate = QuotesService.dateFormat.format(new Date());
        this.imageFileName = imageFilePath;
        this.setQuoteId(UUID.randomUUID().toString().substring(0,15));
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuotedBy getQuotedBy() {
        return quotedBy;
    }

    public void setQuotedBy(QuotedBy quotedBy) {
        this.quotedBy = quotedBy;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    @Override
    public String toString() {
        return "QuoteObject{" +
                "quoteId='" + quoteId + '\'' +
                ", text='" + text + '\'' +
                ", quotedBy=" + quotedBy +
                ", quoteSource='" + quoteSource + '\'' +
                ", entryDate='" + entryDate + '\'' +
                ", imageFileName='" + imageFileName + '\'' +
                '}';
    }
}
