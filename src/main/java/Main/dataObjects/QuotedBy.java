package Main.dataObjects;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * Created by amityaa on 13/03/2018.
 */
public class QuotedBy {

    @NotBlank
    @Size(min = 5, max = 30)
    private String name;
    private QuotedByTypes type = QuotedByTypes.UNKNOWN;

    public QuotedBy(String name){
        this.name = name;
    }

    public QuotedBy(String name, QuotedByTypes type){
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuotedByTypes getType() {
        return type;
    }

    public void setType(QuotedByTypes type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "QuotedBy{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
