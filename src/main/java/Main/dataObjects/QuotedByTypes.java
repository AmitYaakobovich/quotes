package Main.dataObjects;

/**
 * Created by amityaa on 13/03/2018.
 */
public enum QuotedByTypes {

    COMEDIAN,
    PRESIDENT,
    POLITICIAN,
    ACTOR,
    POET,
    AUTHOR,
    ELSE,
    UNKNOWN;

    public static QuotedByTypes convertFromString(String quotedByString){
        if (quotedByString.equalsIgnoreCase("COMEDIAN"))
            return COMEDIAN;
        if (quotedByString.equalsIgnoreCase("PRESIDENT"))
            return PRESIDENT;
        if (quotedByString.equalsIgnoreCase("POLITICIAN"))
            return POLITICIAN;
        if (quotedByString.equalsIgnoreCase("ACTOR"))
            return ACTOR;
        if (quotedByString.equalsIgnoreCase("POET"))
            return POET;
        if (quotedByString.equalsIgnoreCase("AUTHOR"))
            return AUTHOR;
        if (quotedByString.equalsIgnoreCase("ELSE"))
            return ELSE;
        return UNKNOWN;
    }
}
